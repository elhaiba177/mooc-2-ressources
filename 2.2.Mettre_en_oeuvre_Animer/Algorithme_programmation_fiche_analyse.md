- **TP1**:  Instructions de base en Python Shell

    - Matériel didactique :computer: : ordinateur, vidéo projecteur, tableau

    - Ressources didactique :orange_book: : Python Tutorial
    
    - Activités enseignant: Présenter le travail à faire, intervenir pour aider les apprenants (orienter, guider, faciliter), distribuer les apprenants en binôme
    
    - Activités apprenants: Réaliser les activités, discuter les solutions avec les autres binômes, créer une carte mentale qui résume le concept de chaque activité ( *quoi ?*, *qui ?*, *quand ?*, *pourquoi ?*, *Comment ?* )


- **TP2**:  introduction à la programmation avec python turtle

    - Matériel didactique :computer: : ordinateur, vidéo projecteur, tableau
    
    - Ressources didactique :blue_book: : Turtle graphics

    - Activités enseignant: Présenter le travail à faire, intervenir pour aider les apprenants (orienter, guider, faciliter), distribuer les apprenants en binôme

    - Activités apprenants: Réaliser les activités, discuter les solutions avec les autres binômes, créer une carte mentale qui résume le concept de chaque activité ( *quoi ?*, *qui ?*, *quand ?*, *pourquoi ?*, *Comment ?* )
