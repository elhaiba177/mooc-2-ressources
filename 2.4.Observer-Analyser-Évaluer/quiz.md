>**Quiz** :pencil: : introduction à la programmation avec python turtle

**Q1** : Regardez ce code, combien de fois va-t-il boucler ?
 ```python
    for i in range(0, 4):
        turtle.forward(100)
        turtle.left(90)
```
- [ ] 20 
- [ ] 0
- [ ] 3 
- [ ] 4

**Q2** : Regardez le code suivant, quel type de boucle pourrions-nous utiliser pour créer un carré ?
 ```python
    turtle.forward(60)
    turtle.left(90)
```
- [ ] while 
- [ ] for
- [ ] four 
- [ ] whale

**Q3** : Regardez ce code, quelle forme va-t-il prendre ?
 ```python
    turtle.forward(100)
    turtle.left(90)
    turtle.forward(50)
    turtle.left(90)
    turtle.forward(100)
    turtle.left(90)
    turtle.forward(50)
    turtle.left(90)    
```
- [ ] Carré 
- [ ] Hexagone
- [ ] Octogone 
- [ ] Rectangle

**Q4** : Regardez ce code, quelle est la variable ?

 ```python
     for i in range(0, 4):
        turtle.forward(100)
        turtle.left(90)
```
- [ ] 4 
- [ ] 0
- [ ] i 
- [ ] for

**Q5** : Quel mot-clé est utilisé pour définir une fonction ?

- [ ] def 
- [ ] if
- [ ] for
- [ ] while

**Q6** : Quel est le problème avec ce code ?

 ```python
    def carre:
        turtle.forward(100)
        turtle.left(90)
    carre()
```
- [ ] parenthèses manquants
- [ ] Deux points manquants
- [ ] Virgule manquante 
- [ ] orthographe incorrecte

**Q7** : Quel symbole est utilisé pour faire un commentaire ?

- [ ] *
- [ ] /
- [ ] # 
- [ ] []
